import React, { useState } from 'react';
import './assets/styles/main.scss';
import Cookies from 'universal-cookie';
import ReactRouter from './Router';

const App: React.FC = () => {
  const cookies = new Cookies();
  const [theme, setTheme] = useState<string>(cookies.get('theme') ? cookies.get('theme') : 'light');
  /*
  let toggleTheme = () => {
    let newTheme = (theme === 'light') ? 'dark' : 'light';
    cookies.set('theme', newTheme, { path: '/' });
    setTheme(newTheme);
  }
  */

  return (
    <div className={`App ${theme}`}>
      <ReactRouter />
    </div>
  );
};

export default App;
