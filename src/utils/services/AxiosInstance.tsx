import Axios from 'axios';

const AxiosInstance = Axios.create({
  //baseURL: process.env.REACT_APP_ROUTE_API
});
export default AxiosInstance;
